#Passo 1
- digite `ifconfig` ou `ip` a (a parti da versao 18 do ubuntu o if config nao funciona mais) para saber qual o ip da sua VM, normalmente a interface de rede é a eth0.

#Passo 2
- Com o ip em mãos vamos exporta o mesmo o docker host com o seguinte comando `export DOCKER_HOST=tcp://ip_da_VM:2375`, lembrando que o docker host deve se exportado tanto na VM quanto no windows

#Passo 3
- Após isso vamos starta o docker daemon com o seguinte comando `sudo dockerd -H ip_da_VM>2375 -H unix:///var/run/docker.sock`. OBS: para esse comando funcionar o docker não pode estar startado, para saber o status do docker bastar usar o comando
  `sudo service docker status`, para pausar o docker usar o `sudo service docker stop`.

#Passo 4
- Com o daemon starto podemo ir no ambiente windows e gerar a imagem com o  `./mvnw spring-boot:build-image`

#Passo 5
- Quando a imagem for gerada pode entao subir a mesma com docker run nome_image, para isso vamos matar o daemon e starta o docker com o `sudo service docker start`.
- Outra forma de subir o container é deixar o daemon startado e abrir um segundo terminal na VM, caso haja algum erro de conecxão do segundo terminal com o daemon é só rodar o `export DOCKER_HOST=tcp://ip_da_VM:2375` no segundo terminal.

OBS: em caso ao estar a VM uma segunda vez e o ip nao for mais o mesmo bastar usar o seguinte comando `sudo ip addr add ip_da_VM/porta_da_VM(a porta fica do lado do ip pego no passo 1) broadcast ip_VM(trocar os digitos do ultimo ponto por 255) dev eth0 label eth0:1`
um exemplo de como ficar o comando assim `sudo ip addr add 172.22.155.20/20 broadcast 172.22.155.255 dev eth0 label eth0:1`