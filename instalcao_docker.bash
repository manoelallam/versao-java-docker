#!/bin/bash

echo 'INIICANDO INSTALAÇÃO DO DOCKER'

sudo su

echo 'Atualizando sistema'
apt update && sudo apt upgrade -y

echo 'instalando pacotes basicos'
apt install build-essential net-tools dnsutils nmap -y

echo 'Adicionando as chaves oficiais do Docker'
apt install apt-transport-https ca-certificates curl software-properties-common

echo 'Adicionando o Repositório Oficial do Docker'
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

echo 'Adicionando Repositório'
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

echo 'atualizando lista de repostirorios'
apt update

echo 'instalando o docker-ce e o docker-compose'
apt install docker-ce docker-compose -y

echo ' '
echo ' '
echo 'FIM!'

