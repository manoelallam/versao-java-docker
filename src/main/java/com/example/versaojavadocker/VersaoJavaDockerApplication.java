package com.example.versaojavadocker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class VersaoJavaDockerApplication {

	@GetMapping("/")
	String hello() {
		return "java version " + System.getProperty("java.version");
	}

	public static void main(String[] args) {
		SpringApplication.run(VersaoJavaDockerApplication.class, args);
		System.out.println(" ");
		System.out.println("java version " + System.getProperty("java.version"));
	}

}
